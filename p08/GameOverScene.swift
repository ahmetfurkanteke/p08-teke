//
//  WelcomeScene.swift
//  p06
//
//  Created by Ahmet on 4/8/17.
//  Copyright © 2017 Ahmet. All rights reserved.
//

import Foundation
import SpriteKit


class GameOverScene: SKScene{
    let hello_label = SKLabelNode(fontNamed: "duckhunt")
    let play_label = SKLabelNode(fontNamed: "duckhunt")
    let rage_label = SKLabelNode(fontNamed: "duckhunt")
    let high_score_label = SKLabelNode(fontNamed: "Cochin")
    
    var nameField: UITextField!
    
    override func didMove(to view: SKView) {
        
        hello_label.text = "Play Again?"
        hello_label.fontSize = 250
        hello_label.position = CGPoint(x: self.size.width / 2, y: self.size.height * 0.8)
        hello_label.color = .white
        hello_label.name  = "non_clickable"
        self.addChild(hello_label)
        
        let saved = UserDefaults()
        var high_score = saved.integer(forKey: "high_score")
        
        if score > high_score{
            high_score = score
            saved.set(high_score, forKey: "high_score")
        }
        high_score_label.text = "High Score: \(high_score)"
        high_score_label.fontSize = 100
        high_score_label.position = CGPoint(x: self.size.width / 2, y: self.size.height * 0.6)
        high_score_label.color = .white
        high_score_label.name  = "non_clickable"
        self.addChild(high_score_label)
        
        play_label.text = "1 - Normal Mode"
        play_label.fontSize = 150
        play_label.position = CGPoint(x: self.size.width / 2, y: self.size.height * 0.4)
        play_label.color = .white
        play_label.name  = "normal"
        self.addChild(play_label)
        
        
        rage_label.text = "2 - Rage Mode"
        rage_label.fontSize = 150
        rage_label.position = CGPoint(x: self.size.width / 2, y: self.size.height * 0.2)
        rage_label.color = .white
        rage_label.name  = "rage"
        self.addChild(rage_label)
        
        score = 0
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch: AnyObject in touches{
            let node = atPoint(touch.location(in: self))
            let name = node.name
            
            if name == "normal" || name == "rage"{
                mode = name!
                let game_scene = GameScene(size: self.size)
                game_scene.scaleMode = self.scaleMode
                
                let scene_transition = SKTransition.crossFade(withDuration: 0.2)
                self.view!.presentScene(game_scene, transition: scene_transition)
            }
        }
    }
}
