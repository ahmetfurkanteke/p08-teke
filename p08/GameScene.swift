//
//  GameScene.swift
//  p08
//
//  Created by Ahmet on 5/6/17.
//  Copyright © 2017 Ahmet. All rights reserved.
//

import SpriteKit
import GameplayKit
var score = 0

class GameScene: SKScene {
    
    private var label : SKLabelNode?
    private var spinnyNode : SKShapeNode?
    
    let score_label = SKLabelNode(fontNamed: "Cochin")
    let shot_score_label = SKLabelNode(fontNamed: "Cochin")
    
    var dog = SKSpriteNode(imageNamed: "dog_sniff_0.png")
    var duck = SKSpriteNode(imageNamed: "duck_left_0.png")
    
    var shooter = SKSpriteNode(imageNamed: "shoot_0.png")
    var marker = SKSpriteNode(imageNamed: "marker")
    
    let bark_sound = SKAction.playSoundFileNamed("bark.wav", waitForCompletion: false)
    let duck_sound = SKAction.playSoundFileNamed("duck.wav", waitForCompletion: false)
    let laugh_sound = SKAction.playSoundFileNamed("laugh.wav", waitForCompletion: false)
    let shoot_sound = SKAction.playSoundFileNamed("shoot.wav", waitForCompletion: false)
    let start_sound = SKAction.playSoundFileNamed("start.wav", waitForCompletion: false)
    let success_sound = SKAction.playSoundFileNamed("success.wav", waitForCompletion: false)
    
    var new_duck = true
    var is_touchable = false
    var is_success = false
    var is_end = false
    
    var is_left = true
    var is_up = true
    
    var can_move = true
    var last_shoot: TimeInterval = 0
    
    var hit_c = 0 // wall hit counter
    var bullet_c = 2 // bullet counter
    var duck_c = 0
    
    var dog_c = 0
    
    var shoot_speed = 0.15
    
    override func didMove(to view: SKView) {
        set_background()
        if mode == "normal"{
            run(start_sound)
            dog.position = CGPoint(x: self.size.width * 0.148, y: self.size.height  * 0.18)
            dog.zPosition = 4
            dog.xScale = 3
            dog.yScale = 3
            self.addChild(dog)
            
            add_sniffing_dog()
            
            duck.isHidden = true
            self.addChild(duck)
        }
        else{
            shooter.position = CGPoint(x: self.size.width / 2 * 1.04, y: self.size.height * 0.15)
            shooter.zPosition = 4
            shooter.xScale = 2
            shooter.yScale = 2
            self.addChild(shooter)
            
            marker.position = CGPoint(x: self.size.width / 2, y: self.size.height / 2)
            marker.zPosition = 4
            marker.xScale = 0.25
            marker.yScale = 0.25
            self.addChild(marker)
            
        }
        
    }
    
    func set_background(){
        self.backgroundColor = UIColor(red:0.26, green:0.78, blue:0.96, alpha:1.0)
        
        let scene_back = SKSpriteNode(imageNamed: "scene_back.png")
        scene_back.position = CGPoint(x: self.size.width / 2, y: self.size.height * 0.75)
        scene_back.zPosition = 2
        scene_back.xScale = 2.6
        scene_back.yScale = 4
        
        let scene_tree = SKSpriteNode(imageNamed: "scene_tree.png")
        scene_tree.position = CGPoint(x: self.size.width * 0.15, y: self.size.height  * 0.5)
        scene_tree.zPosition = 1
        scene_tree.xScale = 2.6
        scene_tree.yScale = 4
        
        let round_corner_0 = SKShapeNode(rect: CGRect(x: self.size.width * 0.1, y: self.size.height  * 0.01, width: 200, height: 125), cornerRadius: 10)
        round_corner_0.fillColor = UIColor.black
        round_corner_0.lineWidth = 5
        round_corner_0.strokeColor = UIColor.green
        round_corner_0.zPosition = 3
        
        let round_corner_1 = SKShapeNode(rect: CGRect(x: self.size.width * 0.25, y: self.size.height  * 0.01, width: 850, height: 125), cornerRadius: 10)
        round_corner_1.fillColor = UIColor.black
        round_corner_1.lineWidth = 5
        round_corner_1.strokeColor = UIColor.green
        round_corner_1.zPosition = 3
        
        let round_corner_2 = SKShapeNode(rect: CGRect(x: self.size.width * 0.71, y: self.size.height  * 0.01, width: 400, height: 125), cornerRadius: 10)
        round_corner_2.fillColor = UIColor.black
        round_corner_2.lineWidth = 5
        round_corner_2.strokeColor = UIColor.green
        round_corner_2.zPosition = 3
        
        
        self.addChild(scene_back)
        self.addChild(scene_tree)
        self.addChild(round_corner_0)
        self.addChild(round_corner_1)
        self.addChild(round_corner_2)
        
        // fonts
        
        let shot_label = SKLabelNode(fontNamed: "duckhunt")
        shot_label.text = "SHOT"
        shot_label.fontSize = 40
        shot_label.position = CGPoint(x: self.size.width * 0.148, y: self.size.height  * 0.018)
        shot_label.fontColor = UIColor(red:0.26, green:0.78, blue:0.96, alpha:1.0)
        shot_label.zPosition = 4
        
        let hit_label = SKLabelNode(fontNamed: "duckhunt")
        hit_label.text = "HIT"
        hit_label.fontSize = 60
        hit_label.position = CGPoint(x: self.size.width * 0.295, y: self.size.height  * 0.033)
        hit_label.fontColor = .green
        hit_label.zPosition = 4
        
        let score_label_text = SKLabelNode(fontNamed: "duckhunt")
        score_label_text.text = "SCORE"
        score_label_text.fontSize = 40
        score_label_text.position = CGPoint(x: self.size.width * 0.81, y: self.size.height  * 0.018)
        score_label_text.fontColor = .white
        score_label_text.zPosition = 4
        
        score_label.text = "\(score)"
        score_label.fontSize = 60
        score_label.position = CGPoint(x: self.size.width * 0.81, y: self.size.height  * 0.05)
        score_label.fontColor = .white
        score_label.zPosition = 4
        
        shot_score_label.text = ""
        shot_score_label.fontSize = 60
        shot_score_label.fontColor = .white
        shot_score_label.zPosition = 4
        shot_score_label.isHidden = true
        
        self.addChild(shot_score_label)
        self.addChild(shot_label)
        self.addChild(hit_label)
        self.addChild(score_label_text)
        self.addChild(score_label)
        
        // bullets
        
        for i in 0...2 {
            let element = SKSpriteNode(imageNamed: "bullet.png")
            element.position = CGPoint(x: self.size.width * CGFloat((0.13 + Double(i) * 0.018)), y: self.size.height  * 0.063)
            element.zPosition = 4
            element.xScale = 2.6
            element.yScale = 4
            element.name = "bullet_\(i)"
            self.addChild(element)
        }
        
        // score live

        for i in 0...9 {
            let element = SKSpriteNode(imageNamed: "score_live.png")
            element.position = CGPoint(x: self.size.width * CGFloat((0.36 + Double(i) * 0.03)), y: self.size.height  * 0.05)
            element.zPosition = 4
            element.xScale = 3.5
            element.yScale = 4
            element.name = "score_live_\(i)"
            self.addChild(element)
        }
        
        // score dead
        
        for i in 0...9 {
            let element = SKSpriteNode(imageNamed: "score_dead.png")
            element.position = CGPoint(x: self.size.width * CGFloat((0.36 + Double(i) * 0.03)), y: self.size.height  * 0.05)
            element.zPosition = 4
            element.xScale = 3.5
            element.yScale = 4
            element.name = "score_dead_\(i)"
            element.isHidden = true
            self.addChild(element)
        }
    }
    func random_back_ground(){
        self.backgroundColor = UIColor(red:CGFloat(Float(arc4random()) / Float(UINT32_MAX)), green:CGFloat(Float(arc4random()) / Float(UINT32_MAX)), blue:CGFloat(Float(arc4random()) / Float(UINT32_MAX)), alpha:1.0)
    }
    func shooter_shoot(){
        let shoot_atlas = SKTextureAtlas(named: "shoot")
        var shoot_textures: [SKTexture] = []
        
        for i in 0...5{
            shoot_textures.append(shoot_atlas.textureNamed("shoot_\(i).png"))
        }
        
        let shoot_action = SKAction.animate(with: shoot_textures, timePerFrame: shoot_speed)
        shooter.run(SKAction.repeat(shoot_action, count: 1), withKey: "walk")
        run(shoot_sound)
        
    }
    func rage_dog(){
        let random_y = CGFloat(arc4random() % (UInt32(self.size.height) - 100) + 50)
        let random_scale = CGFloat(Float(arc4random()) / Float(UINT32_MAX))
        
        if arc4random() % 2 == 0{
            let rage_dog = SKSpriteNode(imageNamed: "dog_sniff_0.png")
            rage_dog.position = CGPoint(x: -50, y: random_y)
            rage_dog.zPosition = 3
            rage_dog.xScale = 2.6 * random_scale
            rage_dog.yScale = 4 * random_scale
            rage_dog.name = "rage_dog"
            addChild(rage_dog)
            
            let walk_atlas = SKTextureAtlas(named: "dog_sniff")
            var walk_textures: [SKTexture] = []
            
            for i in 0...4{
                walk_textures.append(walk_atlas.textureNamed("dog_sniff_\(i).png"))
            }
            
            let walk_animate = SKAction.animate(with: walk_textures, timePerFrame: shoot_speed)
            let walk_action = SKAction.repeatForever(walk_animate)
            
            let walk_act = SKAction.moveTo(x: self.size.width + 150, duration: 2)
            
            rage_dog.run(walk_action)
            rage_dog.run(walk_act)
        }
        else{
            let rage_dog = SKSpriteNode(imageNamed: "dog_mirror_0.png")
            rage_dog.position = CGPoint(x: self.size.width + 50, y: random_y)
            rage_dog.zPosition = 3
            rage_dog.xScale = 2.6 * random_scale
            rage_dog.yScale = 4 * random_scale
            rage_dog.name = "rage_dog"
            addChild(rage_dog)
            
            let walk_act = SKAction.moveTo(x: -150, duration: 2)
            let walk_atlas = SKTextureAtlas(named: "dog_mirror")
            var walk_textures: [SKTexture] = []
            
            for i in 0...4{
                walk_textures.append(walk_atlas.textureNamed("dog_mirror_\(i).png"))
            }
            
            let walk_animate = SKAction.animate(with: walk_textures, timePerFrame: shoot_speed)
            let walk_action = SKAction.repeatForever(walk_animate)
            
            rage_dog.run(walk_action)
            rage_dog.run(walk_act)

        }
    }
    func clean_dogs(){
        self.enumerateChildNodes(withName: "rage_dog", using: {(node, stop) -> Void in
            if let rage_dog = node as? SKSpriteNode{
                if rage_dog.position.x <= -150 || rage_dog.position.x >= (self.size.width + 150){
                    rage_dog.removeFromParent()
                }
            }
        })
    }
    func add_sniffing_dog(){
        
        
        let sniff_atlas = SKTextureAtlas(named: "dog_sniff")
        var sniff_textures: [SKTexture] = []
        
        for i in 0...4{
            sniff_textures.append(sniff_atlas.textureNamed("dog_sniff_\(i).png"))
        }
        
        let walk_action = SKAction.animate(with: sniff_textures, timePerFrame: 0.25)
        dog.run(SKAction.repeat(walk_action, count: 2), withKey: "walk")
        
        let go_right = SKAction.moveTo(x: self.size.width * 0.35, duration: 2.5)
        let find_texture =  SKAction.setTexture(SKTexture(imageNamed: "dog_find.png"))
        let wait = SKAction.wait(forDuration: 0.5)
        let go_up = SKAction.move(to: CGPoint(x: self.size.width * 0.45, y: self.size.width * 0.25), duration: 0.6)
        let wait_jump = SKAction.wait(forDuration: 3)
        
        
        let fade_out = SKAction.fadeOut(withDuration: 0.1)

        
        let jump_atlas = SKTextureAtlas(named: "dog_jump")
        var jump_textures: [SKTexture] = []
        
        for i in 0...1{
            jump_textures.append(jump_atlas.textureNamed("dog_jump_\(i).png"))
        }
        
        let jump_animate = SKAction.animate(with: jump_textures, timePerFrame: 0.35)
        let jump_action = SKAction.repeat(jump_animate, count: 1)
        
        let sequence = SKAction.sequence([go_right, find_texture, bark_sound, wait, jump_action])
        
        let sequence_jump = SKAction.sequence([wait_jump, go_up, fade_out])
        dog.run(sequence)
        dog.run(sequence_jump)
        
    }
    
    
    func add_duck(){
        
        is_touchable = true
        is_success = false
        
        hit_c = 0
        bullet_c = 2
        
        for i in 0...2{
            self.childNode(withName: "bullet_\(i)")?.isHidden = false
        }
        
        let random_x = CGFloat(arc4random() % UInt32(self.size.width))
        
        duck.removeAllActions()
        
        duck.position = CGPoint(x: random_x, y: self.size.width * 0.22)
        duck.zPosition = 1
        duck.xScale = 2
        duck.yScale = 2
        duck.isHidden = false
        duck.name = "duck"
        
        let random_left_right = arc4random() % 2
        
        run(duck_sound)
        // right
        is_up = true
        if random_left_right == 0{
            is_left = false
            wing_to_right()
        }
        // left
        else{
            is_left = true
            wing_to_left()
        }
        
        // fade in-out score-live
        let fade_in = SKAction.fadeIn(withDuration: 0.2)
        let fade_out = SKAction.fadeOut(withDuration: 0.2)
        // need a better idea?
        let sequence = SKAction.sequence([fade_in, fade_out, fade_in, fade_out, fade_in, fade_out, fade_in, fade_out, fade_in, fade_out, fade_in, fade_out,fade_in, fade_out, fade_in, fade_out,fade_in, fade_out, fade_in, fade_out, fade_in, fade_out, fade_in, fade_out, fade_in, fade_out, fade_in, fade_out,fade_in, fade_out, fade_in, fade_out])
        
        self.childNode(withName: "score_live_\(duck_c)")?.run(sequence)
        
        
        shot_score_label.isHidden = true
    }
    
    func wing_to_left(){
        let fly_atlas = SKTextureAtlas(named: "duck_left")
        var fly_textures: [SKTexture] = []
    
        for i in 0...2{
            fly_textures.append(fly_atlas.textureNamed("duck_left_\(i).png"))
        }
        let fly_action = SKAction.animate(with: fly_textures, timePerFrame: 0.15)
        duck.run(SKAction.repeatForever(fly_action), withKey: "fly")
        
    }
    
    func wing_to_right(){
        
        let fly_atlas = SKTextureAtlas(named: "duck_right")
        var fly_textures: [SKTexture] = []
        
        for i in 0...2{
            fly_textures.append(fly_atlas.textureNamed("duck_right_\(i).png"))
        }
        let fly_action = SKAction.animate(with: fly_textures, timePerFrame: 0.15)
        duck.run(SKAction.repeatForever(fly_action), withKey: "fly")
        
    }
    
    func move_duck(){
        
        var fly_x = SKAction.moveTo(x: duck.position.x - 400, duration: 1)
        if is_left{
            // can go left
            if duck.position.x - 100 > 10{
                fly_x = SKAction.moveTo(x: duck.position.x - 400, duration: 1)
            }
            // can't go left
            else{
                run(duck_sound)
                is_left = false
                fly_x = SKAction.moveTo(x: duck.position.x + 400, duration: 1)
                wing_to_right()
                hit_c += 1
            }
        }
        else{
            // can go right
            if duck.position.x + 100 < self.size.width + 10{
                fly_x = SKAction.moveTo(x: duck.position.x + 400, duration: 1)
            }
            // can't go right
            else{
                run(duck_sound)
                is_left = true
                fly_x = SKAction.moveTo(x: duck.position.x - 400, duration: 1)
                wing_to_left()
                hit_c += 1
            }
        }
        
        var fly_y = SKAction.moveTo(y: duck.position.y - 400, duration: 1)
        
        if is_up{
            // can go up
            if duck.position.y - 100 > self.size.height * 0.25{
                fly_y = SKAction.moveTo(y: duck.position.y - 400, duration: 1)
            }
            // can't go up
            else{
                run(duck_sound)
                is_up = false
                fly_y = SKAction.moveTo(y: duck.position.y + 400, duration: 1)
                hit_c += 1
            }
        }
        else{
            // can go down
            if duck.position.y + 100 < self.size.height - 10{
                fly_y = SKAction.moveTo(y: duck.position.y + 400, duration: 1)
            }
            // can't go down
            else{
                run(duck_sound)
                is_up = true
                fly_y = SKAction.moveTo(y: duck.position.y - 400, duration: 1)
                hit_c += 1
            }
        }
        duck.run(fly_x)
        duck.run(fly_y)
    }
    
    func check_session(){
        hit_c = 0
        bullet_c = 2
        
        
        if !is_success{
            duck.removeAllActions()
        if duck.position.x < self.size.width / 2{
            
            let fly_up = SKAction.move(to: CGPoint(x: -50, y: self.size.height + 50), duration: 2)
            
            let fly_atlas = SKTextureAtlas(named: "duck_top_left")
            var fly_textures: [SKTexture] = []
            
            for i in 0...2{
                fly_textures.append(fly_atlas.textureNamed("duck_top_left_\(i).png"))
            }
            let fly_action = SKAction.animate(with: fly_textures, timePerFrame: 0.15)
            duck.run(SKAction.repeat(fly_action, count: 4))
            duck.run(fly_up)
            
        }
        else{
            
            let fly_up = SKAction.move(to: CGPoint(x: self.size.width + 50, y: self.size.height + 50), duration: 2)
            
            let fly_atlas = SKTextureAtlas(named: "duck_top_right")
            var fly_textures: [SKTexture] = []
            
            for i in 0...2{
                fly_textures.append(fly_atlas.textureNamed("duck_top_right_\(i).png"))
            }
            let fly_action = SKAction.animate(with: fly_textures, timePerFrame: 0.15)
            duck.run(SKAction.repeat(fly_action, count: 4))
            duck.run(fly_up)
        }
        }
        show_dog()
        
    }
    func show_dog(){
        let fade_in = SKAction.fadeIn(withDuration: 0.1)
        let fade_out = SKAction.fadeOut(withDuration: 0.1)
        
        dog.zPosition = 1
        self.childNode(withName: "score_live_\(duck_c)")?.removeAllActions()
        if is_success{
            let find_texture =  SKAction.setTexture(SKTexture(imageNamed: "dog_single.png"))
            let go_up = SKAction.moveTo(y: self.size.width * 0.27, duration: 0.4)
            let go_down = SKAction.moveTo(y: self.size.width * 0.25, duration: 0.4)
            let wait = SKAction.wait(forDuration: 1)
            let wait_long = SKAction.wait(forDuration: 2)
            
            let sequence = SKAction.sequence([wait_long, find_texture, success_sound, fade_in, go_up, wait, go_down, fade_out])
            self.childNode(withName: "score_live_\(duck_c)")?.isHidden = true
            self.childNode(withName: "score_dead_\(duck_c)")?.isHidden = false
            dog.run(sequence)
        }
        else{
            let laugh_atlas = SKTextureAtlas(named: "dog_laugh")
            var laugh_textures: [SKTexture] = []
            
            for i in 0...1{
                laugh_textures.append(laugh_atlas.textureNamed("dog_laugh_\(i).png"))
            }
            
            let laugh_animate = SKAction.animate(with: laugh_textures, timePerFrame: 0.35)
            let laugh_action = SKAction.repeat(laugh_animate, count: 3)

            
            let laugh_texture =  SKAction.setTexture(SKTexture(imageNamed: "dog_laugh_0.png"))
            let go_up = SKAction.moveTo(y: self.size.width * 0.27, duration: 0.4)
            let go_down = SKAction.moveTo(y: self.size.width * 0.25, duration: 0.4)
            let wait = SKAction.wait(forDuration: 1)
            let wait_long = SKAction.wait(forDuration: 2)
            let fade_in = SKAction.fadeIn(withDuration: 0.1)
            let fade_out = SKAction.fadeOut(withDuration: 0.1)
            
            let sequence = SKAction.sequence([wait_long, laugh_sound, laugh_texture, fade_in,  go_up,  laugh_action,   wait, go_down, fade_out])
            
            self.childNode(withName: "score_live_\(duck_c)")?.run(fade_in)
            dog.run(sequence)
        }
        
        duck_c += 1
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch: AnyObject in touches{
            let location = touch.location(in: self)
            let tapped_nodes = nodes(at: location)
            for node in tapped_nodes{
                let name_tapped_node = node.name
                if name_tapped_node == "duck" && is_touchable{
                    can_move = false
                    
                    duck.removeAllActions()
                    
                    let go_down = SKAction.moveTo(y: self.size.height * 0.22, duration: 1)
                    
                    let dead_texture =  SKAction.setTexture(SKTexture(imageNamed: "duck_shot.png"))
                    let wait = SKAction.wait(forDuration: 0.5)
                    
                    
                    let dead_atlas = SKTextureAtlas(named: "duck_dead")
                    var dead_textures: [SKTexture] = []
                    
                    for i in 0...2{
                        dead_textures.append(dead_atlas.textureNamed("duck_dead_\(i).png"))
                    }
                    let dead_animate = SKAction.animate(with: dead_textures, timePerFrame: 0.2)
                    let dead_action = SKAction.repeat(dead_animate, count: 5)
                    
                    let sequence = SKAction.sequence([dead_texture, wait, dead_action])
                    let sequence_2 = SKAction.sequence([wait, go_down])
                    
                    duck.run(sequence)
                    duck.run(sequence_2)
                    
                    is_success = true
                    is_touchable = false
                    
                    shot_score_label.isHidden = false
                    
                    self.childNode(withName: "bullet_\(bullet_c)")?.isHidden = true
                    run(shoot_sound)
                    let recent_score = 500 * (bullet_c + 2)
                    score += recent_score
                    bullet_c -= 1
                    
                    shot_score_label.text = "\(recent_score)"
                    score_label.text = "\(score)"
                    
                    shot_score_label.position = CGPoint(x: duck.position.x, y: duck.position.y - 100)
                    
                    show_dog()
                }
                else if name_tapped_node == "rage_dog"{
                    score += 500
                    score_label.text = "\(score)"
                    node.removeFromParent()
                }
            }
            if mode == "normal"{
                if is_touchable{
                    self.childNode(withName: "bullet_\(bullet_c)")?.isHidden = true
                    bullet_c -= 1
                    run(shoot_sound)
                }
            }
            else{
                
                let move_x = SKAction.moveTo(x: location.x, duration: 0)
                let move_y = SKAction.moveTo(y: location.y, duration: 0)
                
                shooter.run(move_x)
                marker.run(move_x)
                marker.run(move_y)
            }
        }
    }
    
    
    override func update(_ currentTime: TimeInterval) {
        if mode == "normal"{
            if  !dog.hasActions() && new_duck{
                add_duck()
                new_duck = false
            }
            if can_move && duck.hasActions(){
                move_duck()
            }
            
            if !duck.hasActions(){
                new_duck = true
                can_move = true
            }
            if hit_c == 5 || bullet_c == -1{
                NSLog("checked")
                is_touchable = false
                can_move = false
                check_session()
            }
            if duck_c == 10{
                is_end = true
            }
        }
        else{
            clean_dogs()
            
            if currentTime - self.last_shoot > shoot_speed * 5  {
                shoot_speed -= 0.003
                self.last_shoot = currentTime + shoot_speed * 5
                self.shooter_shoot()
                self.random_back_ground()
            }
            if currentTime - self.last_shoot > shoot_speed * 2  {
                self.rage_dog()
                dog_c += 1
            }
            if dog_c == 500{
                is_end = true
            }
        }
        if is_end{
            let game_scene = GameOverScene(size: self.size)
            game_scene.scaleMode = self.scaleMode
            
            let scene_transition = SKTransition.crossFade(withDuration: 0.2)
            self.view!.presentScene(game_scene, transition: scene_transition)
        }
    }
}
